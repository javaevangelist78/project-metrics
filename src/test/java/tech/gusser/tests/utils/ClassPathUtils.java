package tech.gusser.tests.utils;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Objects;

public final class ClassPathUtils {
    private ClassPathUtils() {

    }

    public static File getClassPathFile(String path) {
        Objects.requireNonNull(path, "Path to file must be not null");

        var classLoader = ClassPathUtils.class.getClassLoader();
        var url = classLoader.getResource(path);

        Objects.requireNonNull(url, "Resource not found");

        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException("Can't convert file uri to url", e);
        }
    }
}

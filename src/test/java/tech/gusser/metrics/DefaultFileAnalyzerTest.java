package tech.gusser.metrics;

import org.junit.jupiter.api.Test;
import tech.gusser.imetrics.DefaultFileAnalyzer;
import tech.gusser.tests.utils.ClassPathUtils;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for check work of the default file analyzer
 */
class DefaultFileAnalyzerTest {

    /**
     * This test that default file analyzer can correct analyze files.
     */
    @Test
    void testThatFileCorrectReads() {
        var file = ClassPathUtils.getClassPathFile("test-java-files/Main.java");

        var analyzer = new DefaultFileAnalyzer();
        var analyze = analyzer.analyze(file);

        assertNotNull(analyze);
        assertEquals(analyze.getClassName(), "Main");
        assertEquals(analyze.getImports(), Set.of("java.util.HashSet"));
        assertEquals(analyze.getPack(), "tech.gusser");
    }
}

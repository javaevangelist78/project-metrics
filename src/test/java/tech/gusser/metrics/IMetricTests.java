package tech.gusser.metrics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import tech.gusser.imetrics.FileAnalyzer;
import tech.gusser.imetrics.IMetric;
import tech.gusser.imetrics.PackageNotFoundException;
import tech.gusser.imetrics.RecursiveFolderAnalyzer;
import tech.gusser.tests.utils.ClassPathUtils;

import java.util.Set;
import java.util.stream.Stream;

class IMetricTests {

    private static IMetric iMetric;

    /**
     * Set up all test cases
     */
    @BeforeAll
    static void beforeAll() {
        iMetric = createIMetricInstance();
    }

    /**
     * Test which should check that {@code 'I'} metric calculate correct for given
     * package
     */
    @ParameterizedTest
    @MethodSource("testPackages")
    @DisplayName("Test 'I' metric for different packages in application")
    void testMetricForPackage(String pack, double expectedIndex) {
        var index = iMetric.calculateForPackage(pack);

        Assertions.assertEquals(expectedIndex, index);
    }

    static Stream<Arguments> testPackages() {
        return Stream.of(
                Arguments.of("tech.gusser.app.test", 0.0),
                Arguments.of("tech.gusser.app", 1),
                Arguments.of("tech.gusser.app.inout", 0.5)
        );
    }

    /**
     * Method which create the {@link IMetric} instance
     */
    private static IMetric createIMetricInstance() {
        var analyzer = new RecursiveFolderAnalyzer(
                ClassPathUtils.getClassPathFile("test-java-app/"),
                FileAnalyzer.defaultAnalyzer()
        );
        return new IMetric(analyzer.analyze());
    }

    @Test
    void testPackageNotFoundException() {
        try {
            iMetric.calculateForPackage("tech.gusser.app.main");
            Assertions.fail("Package found, but except that package not found");
        } catch (PackageNotFoundException e) {
            Assertions.assertEquals(Set.of(
                    "tech.gusser.app",
                    "tech.gusser.app.test",
                    "tech.gusser.app.inout"
            ), e.getAvailable());
        }
    }
}

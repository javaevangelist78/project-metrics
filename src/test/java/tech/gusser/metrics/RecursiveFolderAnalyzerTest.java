package tech.gusser.metrics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.gusser.imetrics.FileAnalyzer;
import tech.gusser.imetrics.FileMeta;
import tech.gusser.imetrics.RecursiveFolderAnalyzer;
import tech.gusser.tests.utils.ClassPathUtils;

import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the {@link RecursiveFolderAnalyzer}
 *
 * @author Vyacheslav Gusser
 * @since 0.0.1
 */
class RecursiveFolderAnalyzerTest {
    @Test
    @DisplayName("This test check that recursive file analyzer include all packages")
    void testThatAllPackagesInclude() {
        var analyze = analyzeFolderWithDefaultAnalyzer("test-java-app/");
        assertEquals(3, analyze.size());
        assertTrue(analyze.containsKey("tech.gusser.app"));
        assertTrue(analyze.containsKey("tech.gusser.app.test"));
        assertTrue(analyze.containsKey("tech.gusser.app.inout"));
    }

    @Test
    @DisplayName("Test that directory analyzer ignore all files except java")
    void testThatIgnoreAnyFileExceptJava() {
        var result = analyzeFolderWithDefaultAnalyzer("java-files-with-another/");

        assertNotNull(result);
        assertTrue(result.containsKey("tech.gusser.app"));

        var pckg = result.get("tech.gusser.app");
        assertEquals(1, pckg.size());
        assertEquals("Main", pckg.iterator().next().getClassName());
    }

    @Test
    @DisplayName("This should return empty result when given empty directory")
    void testWhenGivenEmptyDirectory() {
        var result = analyzeFolderWithDefaultAnalyzer("empty/");
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    private Map<String, Set<FileMeta>> analyzeFolderWithDefaultAnalyzer(final String folder) {
        return new RecursiveFolderAnalyzer(
                ClassPathUtils.getClassPathFile(folder),
                FileAnalyzer.defaultAnalyzer()
        ).analyze();
    }
}

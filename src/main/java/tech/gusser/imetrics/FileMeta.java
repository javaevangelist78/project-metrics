package tech.gusser.imetrics;

import java.util.Objects;
import java.util.Set;

/**
 * Information about concrete classfile.
 *
 * @author vaceslavgusser
 * @since 0.0.1
 */
public final class FileMeta {
    /**
     * The name of class in file
     */
    private String className;
    /**
     * Import packages
     */
    private Set<String> imports;
    /**
     * The class package, this may be {@code null}
     */
    private String pack;

    static FileMeta of(String className, Set<String> imports) {
        var ret = new FileMeta();
        ret.setClassName(className);
        ret.setImports(imports);
        return ret;
    }

    public String getClassName() {
        return className;
    }

    /*
     * (non-javadoc)
     *
     * Setter for the class name
     */
    private void setClassName(String className) {
        this.className = className;
    }

    /*
     * (non-javadoc)
     *
     * Getter for imports
     */
    public Set<String> getImports() {
        return imports;
    }

    /*
     * (non-javadoc)
     *
     * Setter for imports
     */
    private void setImports(Set<String> imports) {
        this.imports = Set.copyOf(imports);
    }

    public String getPack() {
        return pack;
    }

    void setPack(String pack) {
        this.pack = pack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileMeta fileMeta = (FileMeta) o;
        return className.equals(fileMeta.className) &&
                imports.equals(fileMeta.imports) &&
                Objects.equals(pack, fileMeta.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className, imports, pack);
    }

    @Override
    public String toString() {
        return "FileMeta{" +
                "className='" + className + '\'' +
                ", imports=" + imports +
                ", pack='" + pack + '\'' +
                '}';
    }
}

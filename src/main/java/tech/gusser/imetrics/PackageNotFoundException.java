package tech.gusser.imetrics;

import java.util.Set;

/**
 * Exception that represent error about missing package with set of available packages (for fix mistake).
 *
 * @author Vyacheslav Gusser
 * @since 0.0.1
 */
@SuppressWarnings("WeakerAccess")
public class PackageNotFoundException extends RuntimeException {
    private final Set<String> available;

    public PackageNotFoundException(String message, Set<String> available) {
        super(message);
        this.available = available;
    }

    public static PackageNotFoundException ofPackage(String pack, Set<String> availablePackages) {
        return new PackageNotFoundException("Package '" + pack + "' not found in analyze",
                Set.copyOf(availablePackages));
    }

    public Set<String> getAvailable() {
        return available;
    }
}

package tech.gusser.imetrics;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This is default implementation of {@link FileAnalyzer}. Which just analyze 
 * file like a text.
 * 
 * @author Vyacheslav Gusser
 * @since 0.0.1
 */
public final class DefaultFileAnalyzer implements FileAnalyzer {
    private static final Pattern IMPORT_PATTERN = Pattern.compile("import .+;");
    private static final Pattern PACKAGE_PATTERN = Pattern.compile("package .+;");
    private static final int PACKAGE_BEGIN_INDEX = 8;
    private static final int IMPORT_BEGIN_INDEX = 7;

    @Override
    public FileMeta analyze(File f) {
        Objects.requireNonNull(f, "File must be not null");

        var content = readFileToString(f);

        var meta = FileMeta.of(
                getClassName(f.getName()),
                findAllImports(content)
        );
        getPackageFromFile(content).ifPresent(meta::setPack);

        return meta;
    }

    private String getClassName(String fileName) {
        return fileName.substring(0, fileName.indexOf('.'));
    }

    /**
     * This should return the package name from the file
     */
    private Optional<String> getPackageFromFile(String content) {
        return tokens(PACKAGE_PATTERN, content, PACKAGE_BEGIN_INDEX).findFirst();
    }

    /**
     * This must find all imports in the file
     */
    private Set<String> findAllImports(String content) {
        return tokens(IMPORT_PATTERN, content, IMPORT_BEGIN_INDEX)
                .collect(Collectors.toSet());
    }

    private Stream<String> tokens(Pattern p, String c, int begin) {
        var ret = Stream.<String>builder();
        var matcher = p.matcher(c);
        while (matcher.find()) {
            var token = matcher.group();
            ret.add(token.substring(begin, token.length() - 1));
        }
        return ret.build();
    }

    /**
     * This should read given file to string and return it
     */
    private String readFileToString(File f) {
        checkFileReadPreconditions(f);
        try {
            return Files.readString(f.toPath(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException("Can't read file to string: " + f.getName(), e);
        }
    }

    /**
     * This should check preconditions which required for read the file.
     */
    private void checkFileReadPreconditions(File f) {
        if (!f.canRead()) {
            throw new RuntimeException("Can't read file: " + f.getName());
        }
        if (!f.isFile()) {
            throw new RuntimeException("Given file is not a file, may be directory?");
        }
    }
}

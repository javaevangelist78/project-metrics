package tech.gusser.imetrics;

import java.io.File;

public interface FileAnalyzer {
    FileMeta analyze(File f);

    static FileAnalyzer defaultAnalyzer() {
        return new DefaultFileAnalyzer();
    }
}

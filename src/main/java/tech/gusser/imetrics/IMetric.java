package tech.gusser.imetrics;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public final class IMetric {
    /**
     * Required for calculate the {@code I} metric
     */
    private final Map<String, Set<FileMeta>> fileMetas;

    @SuppressWarnings("WeakerAccess")
    public IMetric(TargetFolderAnalyzer provider) {
        this(provider.analyze());
    }

    public IMetric(final Map<String, Set<FileMeta>> fileMetas) {
        this.fileMetas = fileMetas;
    }

    public double calculateForPackage(String pack) {
        var inDepCnt = calculateNumberInputDependencies(pack);
        var outDepCnt = calculateNumberOutputDependencies(pack);

        return (double) outDepCnt / (inDepCnt + outDepCnt);
    }

    /**
     * This method should calculate the number of input dependencies to our package.
     */
    private long calculateNumberInputDependencies(String analyzePackage) {
        return fileMetas.entrySet().stream()
                .filter(v -> !analyzePackage.equals(v.getKey()))
                .flatMap(e -> e.getValue().stream())
                .flatMap(v -> v.getImports().stream())
                .filter(v -> packageWithoutClassName(v).equals(analyzePackage))
                .count();
    }

    /**
     * This should return package name without class name. For example, when we have
     * package like {@code tech.gusser.app.Main} then this method should return the {@code
     * tech.gusser.app}
     */
    private String packageWithoutClassName(String pack) {
        return pack.substring(0, pack.lastIndexOf('.'));
    }

    /**
     * This method should calculate number of output dependencies. Calculate imports which
     * contains our package name in another packages
     */
    private long calculateNumberOutputDependencies(String analyzePackage) {
        return getClassesInPackage(analyzePackage).stream()
                .flatMap(v -> v.getImports().stream())
                .filter(v -> !v.equals(analyzePackage)).count();
    }

    /**
     * Should return the classes in given package, this may throw exception when requested
     * package not found in structure
     */
    private Set<FileMeta> getClassesInPackage(String pack) {
        return Optional.ofNullable(fileMetas.get(pack)).orElseThrow(
                () -> PackageNotFoundException.ofPackage(pack, fileMetas.keySet())
        );
    }
}

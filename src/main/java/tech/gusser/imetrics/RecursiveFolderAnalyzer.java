package tech.gusser.imetrics;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public final class RecursiveFolderAnalyzer implements TargetFolderAnalyzer {
    private final File basePath;
    private final FileAnalyzer fileAnalyzer;

    /**
     * Constructor for create files analyzer from base path as a string and default file
     * analyzer
     */
    public RecursiveFolderAnalyzer(String basePath) {
        this(new File(basePath));
    }

    /**
     * Constructor for create recursive files analyzer from file and with default file
     * analyzer
     */
    public RecursiveFolderAnalyzer(File file) {
        this(file, new DefaultFileAnalyzer());
    }

    /**
     * Constructor for create recursive files analyzer from base path as string and custom
     * file analyzer
     */
    public RecursiveFolderAnalyzer(String basePath, FileAnalyzer fileAnalyzer) {
        this(new File(basePath), fileAnalyzer);
    }

    /**
     * Constructor for create recursive files analyzer from file as base path and custom
     * file analyzer
     */
    public RecursiveFolderAnalyzer(File basePath, FileAnalyzer fileAnalyzer) {
        this.basePath = basePath;
        this.fileAnalyzer = fileAnalyzer;
    }

    @Override
    public Map<String, Set<FileMeta>> analyze() {
        var ret = new HashMap<String, Set<FileMeta>>();

        final Queue<File> queue = new LinkedList<>();
        queue.add(basePath);

        while (!queue.isEmpty()) {
            var val = queue.poll();

            if (val.isDirectory()) {
                // ignore all "package-info" files and consume all java files.
                var files = val.listFiles(this::filesFilter);

                queue.addAll(Arrays.asList(Objects.requireNonNull(files)));
            } else {
                // it is file, analyze it
                var analyze = fileAnalyzer.analyze(val);

                var files = ret.compute(analyze.getPack(),
                        (k, v) -> null == v ? new HashSet<>() : v);
                files.add(analyze);
            }
        }

        return ret;
    }

    /**
     * This should filter files, should skip all files except java and also ignore {@code package-info} files
     */
    private boolean filesFilter(final File file) {
        var notPackageInfo = !"package-info.java".equals(file.getName());
        var javaFile = file.getName().endsWith(".java") && file.isFile();

        return (notPackageInfo && javaFile) || file.isDirectory();
    }
}

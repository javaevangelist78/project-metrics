package tech.gusser.imetrics;

import java.util.Map;
import java.util.Set;

public interface TargetFolderAnalyzer {
    Map<String, Set<FileMeta>> analyze();
}

package tech.gusser.imetrics;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * This application should calculate {@code I} metric for selected package by user.
 *
 * @author gusservv
 * @since 0.0.1
 */
public final class App {
    private static final String JAVA_PACKAGE_REGEX = "^[a-z][a-z0-9_]*(\\.[a-z0-9_]+)+[0-9a-z_]$";

    /**
     * Location of the project in filesystem
     */
    private final String projectPath;
    /**
     * Package for calculate the {@code 'I'} metric
     */
    private final String packg;

    /**
     * Primary constructor which should accept all required arguments.
     */
    private App(final String projectPath, final String packg) {

        this.projectPath = projectPath;
        this.packg = packg;
    }

    @SuppressWarnings("WeakerAccess")
    public void run() {
        checkPreconditions();

        var index = new IMetric(
                new RecursiveFolderAnalyzer(
                        projectPath, new DefaultFileAnalyzer()
                )
        ).calculateForPackage(packg);

        System.out.println("'I' index for package '" + packg + "' = " + index);
    }

    /**
     * This must check preconditions for all arguments from command line
     */
    private void checkPreconditions() {
        if (!isJavaPackage(packg)) {
            throw new RuntimeException("Second argument must be java package, " +
                    "but got: " + packg);
        }
        if (!Files.exists(Path.of(projectPath))) {
            throw new RuntimeException("Path with application does not exists");
        }
    }

    /**
     * This should return {@code true} when given string is not empty and it matches to
     * java package regex
     */
    private boolean isJavaPackage(final String v) {
        return !v.isBlank() && v.matches(JAVA_PACKAGE_REGEX);
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Enter all required arguments: first argument should be path to " +
                    "the project for analyze and second argument should be p ackage for calculate 'I' metric");
            return;
        }
        runApp(args);
    }

    /**
     * This method should instantiate app object and run it
     */
    private static void runApp(final String[] args) {
        try {
            new App(args[0], args[1]).run();
        } catch (PackageNotFoundException e) {
            System.err.println(e.getMessage());
            System.err.println("Available packages:");
            System.err.println(String.join("\n", e.getAvailable()));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
